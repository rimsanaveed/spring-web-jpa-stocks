package com.citi.training.stocks;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

public class StockController {
	
	@Autowired
    StockDao stockDao;
	
	@RequestMapping(method=RequestMethod.GET)
	public Iterable<Stock> findAll(){
		LOG.info("GET to finAll() method");
    	LOG.debug("Some message...");
		return stockDao.findAll();
	}
	
	 @RequestMapping(value="/{id}", method=RequestMethod.GET)
	public Stock findById(@PathVariable long id) {
		return stockDao.findById(id).get();
	}
	
	@RequestMapping(method=RequestMethod.POST)
	public void create (@RequestBody Stock stock) {
		stockDao.save(stock);
	}
	@RequestMapping(value="/{id}", method=RequestMethod.DELETE)
	public void deleteById(@PathVariable long id) {
		stockDao.deleteById(id);
	}
}
